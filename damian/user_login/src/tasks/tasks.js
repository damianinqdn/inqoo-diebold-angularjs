const tasks = angular.module('tasks', [])

angular.module('tasks', [])
    .component('tasks', {
        template: /** html */`
     `
        ,
        
        controller($scope) {
            
            this.$onInit() = () => {
                this.mode = "show"

                this.searchValue = "";

                this.selected = (task) => {
                    this.selectedTask = task
                }

                this.tasks = [{
                    id: "123",
                    name: "Task 123",
                    completed: true
                }, {
                    id: "234",
                    name: "Task 234",
                    completed: false
                }, {
                    id: "345",
                    name: "Task 345",
                    completed: true
                }];
            }

            this.edit = () => {
                this.mode = "edit"
                this.taskCopy = { ...this.task }
            }
        
            this.deleteTask = (index) => {
                this.tasks.splice(index, 1)
            }
        
            this.delete = () => {
                this.mode = "show"
            }
        
            this.save = () => {
                this.task = { ...this.taskCopy }
            }
        
            this.addTask = () => {
                this.tasks.push({
                    id: Date.now.toString(),
                    name: this.fastTaskName
                })
            }
        
            this.search = (event) => {
                updateFilteredProducts()
            }
        
            this.$watchCollection('tasks', (newVal, oldVal) => {
                updateFilteredProducts()
                updateSumOfItems()
            })
        
            function updateFilteredProducts() {
                this.filteredTasks = this.tasks.filter((task) => {
                    return task.name.includes(this.searchValue)
                })
            }
        
            function updateSumOfItems() {
                let sum = 0;
                this.filteredTasks.forEach(() => {
                    sum++
                });
                this.sumValue = sum;
            }
        },
        controllerAs: 'TasksCtrl'
    })

// tasks.controller("TasksCtrl", ($scope) => {
//     $scope.userTasks = vm
//     $vm.mode = "show"

//     $vm.searchValue = "";

//     $vm.selected = (task) => {
//         $vm.selectedTask = task
//     }

//     $vm.tasks = [{
//         id: "123",
//         name: "Task 123",
//         completed: true
//     }, {
//         id: "234",
//         name: "Task 234",
//         completed: false
//     }, {
//         id: "345",
//         name: "Task 345",
//         completed: true
//     }];

//     $vm.edit = () => {
//         $vm.mode = "edit"
//         $vm.taskCopy = { ...$vm.task }
//     }

//     $vm.deleteTask = (index) => {
//         $vm.tasks.splice(index, 1)
//     }

//     $vm.delete = () => {
//         $vm.mode = "show"
//     }

//     $vm.save = () => {
//         $vm.task = { ...$vm.taskCopy }
//     }

//     $vm.addTask = () => {
//         $vm.tasks.push({
//             id: Date.now.toString(),
//             name: $vm.fastTaskName
//         })
//     }

//     $vm.search = (event) => {
//         updateFilteredProducts()
//     }

//     $vm.$watchCollection('tasks', (newVal, oldVal) => {
//         updateFilteredProducts()
//         updateSumOfItems()
//     })

//     function updateFilteredProducts() {
//         $vm.filteredTasks = $vm.tasks.filter((task) => {
//             return task.name.includes($vm.searchValue)
//         })
//     }

//     function updateSumOfItems() {
//         let sum = 0;
//         $vm.filteredTasks.forEach(() => {
//             sum++
//         });
//         $vm.sumValue = sum;
//     }

//     // function updateSumOfItems() {
//     //     $vm.filteredTasks.reduce(function(a, b) {
//     //         return a + b;
//     //     },0)
//     // }
//     // $vm.sumValue = updateSumOfItems()
// })

