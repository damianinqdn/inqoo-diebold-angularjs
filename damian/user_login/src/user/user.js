
const users = angular.module('users', ['users.service'])

angular.module('users').controller('UsersPageCtrl', ($scope, UsersService) => {
  console.log('Hello UsersPageCtrl', UsersService)
  $scope.users = UsersService.getUsers()
  
  // get user to edit, select user
  $scope.selected = (user) => {
    $scope.selectedUser = user
  }
  
  // user edition, show edit section
  $scope.userEditSection = false;

  $scope.editUser = (user) => {
    $scope.userEditSection = true;
    console.log("Edit User", user)
  }
  
  
  
})


angular.module('users.service', []).service('UsersService', function ($http) {
  console.log('Hello UsersService')

  this._users = [
    // { id: "123", username: 'Admin' },
    // { id: "125", username: 'Admin2' }
  ]

  this.fetchUsers = () => {
    return $http.get('http://localhost:3000/users').then(res => {
      return (this._users = res.data)
    })
  }
  this.fetchUsers()

  this.fetchUsersById = (id) => {
    return $http.get(`http://localhost:3000/users/${id}`).then(res => res.data)
  } 
  //this._users = this.fetchUsers()


  this.getUsers = () => {
    console.log(this._users);
    return this._users }
  this.updateUser = () => { }
  this.getUserById = (id) => {
    const result = this.fetchUsersBYId(id)
    console.log("Search user by ID") 
    return result
  }

})