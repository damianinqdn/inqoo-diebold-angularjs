

const userSettings = angular.module("UserSettings", [])

userSettings.controller("UserSettingsCtrl", ($scope, UsersService) => {
    console.log("Hello User Settings Controller", $scope)

const vm = $scope.usersPage = {}
$scope.users2 = UsersService.getUsers()

vm.selected = null
vm.draft

vm.selected = (id) => {
    vm.selected = UsersService.getUserById(id);
    vm.mode = 'details'
}

vm.edit = () => {
    vm.draft = {... vm.selected}
    vm.mode = 'edit'
}

vm.save = (draft) => {
    const saved = UsersService.save(draft)
    vm.users = UsersService.getUsers()
}


})