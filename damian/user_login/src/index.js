

const app = angular.module('myApp', ["tasks", "users", "UserSettings", "ui.router"])

// constant -> view changing   - not finished 
// app.constant('PAGES', [
//     { name: 'tasks', label: 'Tasks', tpl:'views/tasks-view.tpl.html' },
//     { name: 'users', label: 'Users', tpl:'views/users-view.tpl.html' },
// ])
app.config( function($stateProvider, $urlRouterProvider) {
    // $urlRouterProvider.otherwise('/')
    $stateProvider
        .state({name: 'users', url: '/users', component: 'users'})
        .state({name: 'tasks', url: '/tasks', component: 'tasks'})
        .state({name: 'settings', url: '/settings', templateUrl: 'views/user_settings_template.html'})
    })


app.run(function ($state) {
    $state.go('tasks')
  })

app.controller('AppCtrl', ($scope)=>{

    $scope.userPage = false;
    $scope.taskPage = false;
    $scope.userSettingsPage = false;

    $scope.displayUserPage = () => {
        console.log("Display User Page")
        $scope.userPage = true;
        $scope.taskPage = false;
        $scope.userSettingsPage = false;
    }

    $scope.displayTaskPage = () => {
        console.log("Display Task Page")
        $scope.userPage = false;
        $scope.taskPage = true;
        $scope.userSettingsPage = false;

    }
    $scope.displayUserSettingsPage = () => {
        console.log("Display Settings User Page")
        $scope.userSettingsPage = true;
        $scope.userPage = false;
        $scope.taskPage = false;
    }


    $scope.title = 'MyApp'

    $scope.user = { name : 'Guest'}
    $scope.isLoggedIn = false

    $scope.login = () => {
        $scope.user.name = 'Admin'
        $scope.isLoggedIn = true
    }

    $scope.logout = () => {
        $scope.user.name = 'Guest'
        $scope.isLoggedIn = false
    }
})