angular.module('settings', [])
angular.module('settings').directive("appAlert", function () {
    return {
        scope: {messageTxt: '@message', type: '@type'},
        template: /* html */ `<div class="alert alert-{{type}}" ng-show="open">
        <span class="close float-end" ng-click="open = false">&times;</span>
        <div>{{messageTxt}}</div> 
        </div>
        `,
        controller($scope) {
            $scope.open = true;
            $scope.close  = () => {
                $scope.open = false
                $scope.onDismiss()
            }
        }
    }
})

angular.module('settings').controller('SettingsCtrl', function () {
    
})