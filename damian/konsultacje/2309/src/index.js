
const app = angular.module('app', ['InvoiceEditor', 'Invoice','clients', 'orders'])

angular.module('orders', []).controller('OrderCtrl', ($scope)=>{
  const vm = $scope.orders = {}
})


app.controller('OrdersCtrl', ($scope) => {
  const vm = $scope.orders = {}

  vm.selectedClient = client => console.log('client order:', client)
})

const invoiceEditor = angular.module('InvoiceEditor', [])
invoiceEditor.controller('InvoiceEditorCtrl', (
  $scope, InvoiceService
) => {

  $scope.invoice = InvoiceService.getInvoice()

  $scope.addPosition = (index) => { 
    InvoiceService.addPosition(index)
  }

  $scope.removePosition = (index) => { 
    InvoiceService.removePosition(index)
  }

})


angular.module('clients',[])
.service('ClientsService',function ClientsService(){

  this._clients = [
    {id:"123", name:"ClientA 123", address:"ul. A", nip:"123"},
    {id:"234", name:"ClientB 234", address:"ul. B", nip:"234"},
    {id:"345", name:"ClientC 345", address:"ul. C", nip:"345"},
  ]
  this.search = (query) => {
    return this._clients.filter(c => c.name.toLowerCase().includes(query.toLowerCase()))
  }

})
.controller('ClientLookupCtrl', ($scope, ClientsService) => {
  const vm = $scope.lookup = {}
  vm.query = ''
  vm.results = []

  document.addEventListener('click', (event) => {
    event.preventDefault()
  })

  vm.search = () => {
    console.log('test', vm.query)
    vm.results = ClientsService.search(vm.query)
  }

  // console .log (selectedClient)
})

// document.addEventListener('load', () => angular.bootstrap())