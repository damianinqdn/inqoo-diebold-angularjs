
const app = angular.module('app', ['InvoiceEditor'])

const invoiceEditor = angular.module('InvoiceEditor', [])
invoiceEditor.controller('InvoiceEditorCtrl', ($scope) => {

  const invoice = {
    positions: [
      {
        id: "123",
        title: 'Pozycja 123',
        netto: 1000,
        tax: 23,
        brutto: 1230
      },
      {
        id: "234",
        title: 'Pozycja 234',
        netto: 2000,
        tax: 23,
        brutto: 2460
      },
      {
        id: "456",
        title: 'Pozycja 456',
        netto: 100,
        tax: 23,
        brutto: 123
      },
    ], summary : {
      total: 0
    }
  }
  $scope.invoice = invoice

  $scope.addPosition = (index) => {
    console.log("add position")
    invoice.positions.splice(index, 0, {brutto: 0})

   }
  $scope.removePosition = (index) => { 
    console.log("position removed")
    invoice.positions.splice(index, 1)
    $scope.getTotal()
  }

  // $scope.getTotal = () => {
  //   invoice.summary.total = invoice.positions.reduce(
  //     (sum, position)=>{
  //       return sum + position.brutto
  //     },0
  //   )
  // }
  // $scope.getTotal()

  $scope.getTotal = () => {
    let total = 0;
    invoice.positions.forEach(i => {
      total += i.brutto
    });
    invoice.summary.total = total;
  }
  $scope.getTotal()

})

