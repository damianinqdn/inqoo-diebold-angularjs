
angular.module('users', [])


angular.module('users').component('usersPage', {
  bindings: {},
  template:/* html */`<div>
    <h1>{{ $ctrl.title | uppercase }}</h1>
    <div class="row">
      <div class="col">
      
      <!-- <pre>{{ $ctrl.users | json:2 }}</pre> -->
      <users-list users="$ctrl.users"></users-list>  
      </div>
      <div class="col">
        <pre>{{ $ctrl.selected | json:2 }}</pre> 
        <user-details></user-details>  
        <user-editor></user-editor>  
      </div>
    </div>
  </div>`,
  controller: function (UsersService, $timeout) {
    this.title = 'Users Page'
    this.users = []
    this.selected = null
    
    $timeout(() => {
      UsersService.fetchUsers().then(data => {
        this.users = data
        this.selected = this.users[0]
      })
    }, 2000)

  },
})



angular
  .module('users')
  .service('UsersService', UsersService)

function UsersService($q, $http) {
  this.fetchUsers = () => {
    return $q.resolve([
      { id: '123', name: 'Bardzo dluuuuga nazwao uzytkownika 123' },
      { id: '234', name: 'User 234' },
      { id: '345', name: 'User 345' },
    ])
  }
}


angular.module('users').component('usersList', {
  bindings: {
    users: '<'
  },
  template:/* html */`<div> Users List
    <div class="list-group">
      <div class="list-group-item" ng-repeat="user in $ctrl.users">
        {{user.name}}
      </div>
    </div>
  </div>`,
  controller() {

  }

})



angular.module('users').component('userDetails', {
  bindings: {
  },
  template:/* html */`<div> 
      <dl><dt>Name:</dt><dd>Username here!</dd></dl>
  </div>`
})